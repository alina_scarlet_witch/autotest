﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using NUnit.Framework;

namespace test.POM
{
        public class WaitForManagerCallPage
    {
        private IWebDriver _driver;
        public By dontMissCallLabel = By.XPath("/html/body/div/div[2]/div/div/div[2]/div[1]");
        public By descriptionLabel = By.XPath("/html/body/div/div[2]/div/div/div[2]/div[2]");
        public WaitForManagerCallPage(IWebDriver driver)   //конструктор
        {
            this._driver = driver;
        }
        public IWebElement FindDontMissCallLabel()  //найти главную надпись на главной странице
        {
            return _driver.FindElement(dontMissCallLabel);
        }

        public IWebElement FindDescriptionLabel()  //получить текст главного Label1
        {
            return _driver.FindElement(descriptionLabel);
        }

        public string GetTextFromDontMissCallLabel()  //получить текст главного Label1
        {
            return FindDontMissCallLabel().Text;
        }
        public string GetTextFromDescriptionLabel()  //получить текст главного Label1
        {
            return FindDescriptionLabel().Text;
        }
    }
}
