﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;

namespace test.POM
{
    public class MainPage
    {
        private IWebDriver _driver;
        public By mainLabel1 = By.XPath("/html/body/main/div/div[1]/div/h1");
        public By getGrandFormName = By.XPath("/html/body/main/section[2]/div/form/input[3]");
        public By getGrandFormPhone = By.XPath("/html/body/main/section[2]/div/form/input[4]");
        public By getGrandFormEmail = By.XPath("/html/body/main/section[2]/div/form/input[5]");
        public By getGrandFormCity = By.XPath("/html/body/main/section[2]/div/form/div/select");
        public By getGrandFormDniproCity = By.XPath("/html/body/main/section[2]/div/form/div/select/option[4]");
        public By getGrandFormStPetersburgCity = By.XPath("/html/body/main/section[2]/div/form/div/select/option[2]");
        public By getGrandFormKyivCity = By.XPath("/html/body/main/section[2]/div/form/div/select/option[5]");
        public By getGrandFormKharkovCity = By.XPath("/html/body/main/section[2]/div/form/div/select/option[3]");
        public By getGrandFormBakuCity = By.XPath("/html/body/main/section[2]/div/form/div/select/option[6]");

        public By getGrandButton = By.XPath("/html/body/main/section[2]/div/form/button");

        public By coursesButton = By.XPath("/html/body/header/div/div[2]/nav/ul/li[1]/a");
        public By trainingСenterButton = By.XPath("/html/body/header/div/div[2]/nav/ul/li[2]/a");
        public By blogInTrainingСenterButton = By.XPath("/html/body/header/div/div[2]/nav/ul/li[2]/ul/li[1]/a");
        public By webinarsIntrainingСenterButton = By.XPath("/html/body/header/div/div[2]/nav/ul/li[2]/ul/li[2]/a");
        public By glossaryIntrainingСenterButton = By.XPath("/html/body/header/div/div[2]/nav/ul/li[2]/ul/li[3]/a");
        
        public By aboutUsButton = By.XPath("/html/body/header/div/div[2]/nav/ul/li[3]/a");
        public By graduatesInAboutUsButton = By.XPath("/html/body/header/div/div[2]/nav/ul/li[3]/ul/li[1]/a");
        public By newsInAboutUsButton = By.XPath("/html/body/header/div/div[2]/nav/ul/li[3]/ul/li[2]/a");

        public By contactsButton = By.XPath("/html/body/header/div/div[2]/nav/ul/li[4]/a");

        public By languageRU = By.XPath("/html/body/header/div/div[2]/div/ul/li[2]/a");
        public By languageUA = By.XPath("/html/body/header/div/div[2]/div/ul/li[1]/a");
        public By languageAZ = By.XPath("/html/body/header/div/div[2]/div/ul/li[3]/a");
        public By languageEN = By.XPath("/html/body/header/div/div[2]/div/ul/li[4]/a");

        public By cityStPetersburg = By.XPath("/html/body/main/div/div[4]/div/a[1]");
        public By cityKyiv = By.XPath("/html/body/main/div/div[4]/div/a[2]");
        public By cityDnepr = By.XPath("/html/body/main/div/div[4]/div/a[3]");
        public By cityKharkov = By.XPath("/html/body/main/div/div[4]/div/a[4]");
        public By cityBaku = By.XPath("/html/body/main/div/div[4]/div/a[5]");

        public By courseСSharpIncityStPetersburg = By.XPath("/html/body/main/section[1]/div/div/ul/li[1]/div/div[2]/a[1]");
        public By courseСSharpIncityBaku = By.XPath("/html/body/main/section[1]/div/div/ul/li[1]/div/div[2]/a[2]");
        public By courseJavaIncityKharkov = By.XPath("/html/body/main/section[1]/div/div/ul/li[2]/div/div[2]/a[1]");
        public By courseJavaIncityKyiv = By.XPath("/html/body/main/section[1]/div/div/ul/li[2]/div/div[2]/a[2]");
        public By courseQaAtIncityKharkov = By.XPath("/html/body/main/section[1]/div/div/ul/li[3]/div/div[2]/a[1]");
        public By courseQaAtIncityDnepr = By.XPath("/html/body/main/section[1]/div/div/ul/li[3]/div/div[2]/a[2]");
        public By courseQaAtIncityKyiv = By.XPath("/html/body/main/section[1]/div/div/ul/li[3]/div/div[2]/a[3]");
        public By courseQaAtIncityBaku = By.XPath("/html/body/main/section[1]/div/div/ul/li[3]/div/div[2]/a[4]");
        public By courseFrontEndIncityBaku = By.XPath("/html/body/main/section[1]/div/div/ul/li[4]/div/div[2]/a");

        public By readButton = By.XPath("/html/body/main/section[5]/div/a");
        public By moreNewsButton = By.XPath("/html/body/main/section[6]/div/a");
        public By signUpForCourseButton = By.XPath("/html/body/div[3]/button");

        public MainPage(IWebDriver driver)   //конструктор
        {
            this._driver = driver;
        }

        //public IWebElement FindMainLabel1()  //найти главную надпись на главной странице
        //{
        //    return _driver.FindElement(mainLabel1);
        //}
       

        //public string GetTextFromLabel1()  //получить текст главного Label1
        //{
        //    return new UserMethod(_driver).FindMainLabel1(mainLabel1).Text;
        //}

        public MainPage EnterTextToNameField(string name)  //вводим имя в строку введите имя на запись курса
        {
            _driver.FindElement(getGrandFormName).SendKeys(name);
            return this;
        }

        public MainPage EnterTextToPhoneField(string phone) //вводим телефон в строку введите телефон на запись курса
        {
            _driver.FindElement(getGrandFormPhone).SendKeys(phone);
            return this;
        }

        public MainPage EnterTextToEmailField(string email) //вводим email в строку введите email на запись курса
        {
            _driver.FindElement(getGrandFormEmail).SendKeys(email);
            return this;
        }
        public MainPage ClickOnCityDropDown()  //Выбор города на запись курса
        {
            _driver.FindElement(getGrandFormCity).Click();
            return this;
        }
        public MainPage ClickOnDniproCity()  //Выбор города Днепр на запись курса
        {
            _driver.FindElement(getGrandFormDniproCity).Click(); 
            return this;
        }


        public MainPage ClickOnStPetersburgCity()  //Выбор города питер на запись курса
        {
            _driver.FindElement(getGrandFormStPetersburgCity).Click();
            return this;
        }
        public MainPage ClickOnKyivCity()  //Выбор города Киев на запись курса
        {
            _driver.FindElement(getGrandFormKyivCity).Click();
            return this;
        }
        public MainPage ClickOnKharkovCity()  //Выбор города Харьков на запись курса
        {
            _driver.FindElement(getGrandFormKharkovCity).Click();
            return this;
        }
        public MainPage ClickOnBakuCity()  //Выбор города Баку на запись курса
        {
            _driver.FindElement(getGrandFormBakuCity).Click();
            return this;
        }
        public WaitForManagerCallPage ClickOnetGrandButton()  //Кнопка Получить грант
        {
            _driver.FindElement(getGrandButton).Click();
            return new WaitForManagerCallPage(_driver);
        }
        //////////////////////////////////////////////////////////////////////////////////////////////     

        //public Courses ClickСoursesButton()  //клик на кнопке курсы на главной странице
        //{
        //    _driver.FindElement(coursesButton).Click();
        //    return new Courses(_driver);
        //}

        //public ???? ClickTrainingСenterButton()  //клик на кнопке обучающий центр на главной странице
        //{
        //    _driver.FindElement(trainingСenterButton).Click();
        //    return new ???? (_driver);
        //}
        //public ???? ClickBlogInTrainingСenterButton() //клик на кнопке блог в обучающем центре на главной странице
        //{
        //    _driver.FindElement(trainingСenterButton).FindElement(blogInTrainingСenterButton).Click();
        //    return new ???? (_driver);
        //}
        //public ???? ClickWebinarsIntrainingСenterButton()  //найти кнопку вебинары в обучающем центре на главной странице
        //{
        //    _driver.FindElement(trainingСenterButton).FindElement(webinarsIntrainingСenterButton).Click();
        //    return new ???? (_driver);
        //}
        //public ???? ClickGlossaryIntrainingСenterButton()  //найти кнопку глоссарий в обучающем центре на главной странице
        //{
        //    _driver.FindElement(trainingСenterButton).FindElement(glossaryIntrainingСenterButton).Click();
        //    return new ???? (_driver);
        //}


        //public ???? ClickAboutUsButton()  //найти кнопку о нас на главной странице
        //{
        //    _driver.FindElement(aboutUsButton).Click();
        //    return new ???? (_driver);
        //}
        //public ???? ClickGraduatesInAboutUsButton()  //найти кнопку выпускники в о нас на главной странице
        //{
        //    _driver.FindElement(aboutUsButton).FindElement(graduatesInAboutUsButton).Click();
        //    return new ???? (_driver);
        //}
        //public ???? ClickNewsInAboutUsButton()  //найти кнопку новости в о нас на главной странице
        //{
        //    _driver.FindElement(aboutUsButton).FindElement(newsInAboutUsButton).Click();
        //    return new ???? (_driver);
        //}
        //public ???? ClickСontactsButton()  //найти кнопку контакты главной странице
        //{
        //    _driver.FindElement(contactsButton).Click();
        //    return new ???? (_driver);
        //}


        ////public IWebElement FindLanguageRU()  //найти кнопку русского языка на главной странице
        ////{
        ////    return _driver.FindElement(languageRU);
        ////}
        ////public IWebElement FindLanguageUA()  //найти кнопку UA языка на главной странице
        ////{
        ////    return _driver.FindElement(languageUA);
        ////}
        ////public IWebElement FindLanguageAZ()  //найти кнопку AZ языка на главной странице
        ////{
        ////    return _driver.FindElement(languageAZ);
        ////}
        ////public IWebElement FindLanguageEN()  //найти кнопку EN языка на главной странице
        ////{
        ////    return _driver.FindElement(languageEN);
        ////}


        //public ???? ClickСityStPetersburg()  //найти город StPetersburg на главной странице
        //{
        //    _driver.FindElement(cityStPetersburg).Click();
        //    return new ???? (_driver);
        //}
        //public ???? ClickСityKyiv()  //найти город Kyiv на главной странице
        //{
        //    _driver.FindElement(cityKyiv).Click();
        //    return new ???? (_driver);
        //}
        //public ???? ClickСityDnepr()  //найти город Dnepr на главной странице
        //{
        //    _driver.FindElement(cityDnepr).Click();
        //    return new ???? (_driver);
        //}
        //public ???? ClickСityKharkov()  //найти город Kharkov на главной странице
        //{
        //    _driver.FindElement(cityKharkov).Click();
        //    return new ???? (_driver);
        //}
        //public ???? ClickСityBaku()  //найти город Baku на главной странице
        //{
        //    _driver.FindElement(cityBaku).Click();
        //    return new ???? (_driver);
        //}


        //public ???? ClickСourseСSharpIncityStPetersburg()  //найти курс c# в городе StPetersburg на главной странице
        //{
        //    _driver.FindElement(courseСSharpIncityStPetersburg).Click();
        //    return new ???? (_driver);
        //}
        //public ???? ClickСourseСSharpIncityBaku()  //найти курс c# в городе Baku на главной странице
        //{
        //    _driver.FindElement(courseСSharpIncityBaku).Click();
        //    return new ???? (_driver);
        //}
        //public ???? ClickСourseJavaIncityKharkov()  //найти курс Java в городе Kharkov на главной странице
        //{
        //    _driver.FindElement(courseJavaIncityKharkov).Click();
        //    return new ???? (_driver);
        //}
        //public ???? ClickСourseJavaIncityKyiv()  //найти курс Java в городе Kyiv на главной странице
        //{
        //    _driver.FindElement(courseJavaIncityKyiv).Click();
        //    return new ???? (_driver);
        //}
        //public ???? ClickСourseQaAtIncityKharkov()  //найти курс QaAt в городе Kharkov на главной странице
        //{
        //    _driver.FindElement(courseQaAtIncityKharkov).Click();
        //    return new ???? (_driver);
        //}
        //public ???? ClickСourseQaAtIncityDnepr()  //найти курс QaAt в городе Dnepr на главной странице
        //{
        //    _driver.FindElement(courseQaAtIncityDnepr).Click();
        //    return new ???? (_driver);
        //}
        //public ???? ClickСourseQaAtIncityKyiv()  //найти курс QaAt в городе Kyiv на главной странице
        //{
        //    _driver.FindElement(courseQaAtIncityKyiv).Click();
        //    return new ???? (_driver);
        //}
        //public ???? ClickСourseQaAtIncityBaku()  //найти курс QaAt в городе Baku на главной странице
        //{
        //    _driver.FindElement(courseQaAtIncityBaku).Click();
        //    return new ???? (_driver);
        //}
        //public ???? ClickСourseFrontEndIncityBaku()  //найти курс FrontEnd в городе Baku на главной странице
        //{
        //    _driver.FindElement(courseFrontEndIncityBaku).Click();
        //    return new ???? (_driver);
        //}


        //public ???? ClickReadButton()  //найти кнопку читать на главной странице
        //{
        //    _driver.FindElement(readButton).Click();
        //    return new ???? (_driver);
        //}
        //public ???? ClickMoreNewsButton()  //найти кнопку больше новостей на главной странице
        //{
        //    _driver.FindElement(moreNewsButton).Click();
        //    return new ???? (_driver);
        //}
        //public ???? ClickSignUpForCourseButton()  //найти кнопку записаться на курс на главной странице
        //{
        //    _driver.FindElement(signUpForCourseButton).Click();
        //    return new ???? (_driver);
        //}
        /////////////////////////////////////////////////////////////////////////////////////////
        public MainPage ClickSignUpForCourseButton()  //найти кнопку записаться на курс на главной странице
        {
            Actions action = new Actions(_driver);
            action.MoveToElement((IWebElement)aboutUsButton).Perform();
            _driver.FindElement(graduatesInAboutUsButton).Click();
            return new MainPage(_driver);
        }
        
    }
}
