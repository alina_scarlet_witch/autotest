﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using NUnit.Framework;

namespace test.POM
{
    class TrainingCenter
    {
        private IWebDriver _driver;
        public By mainLabel = By.XPath("/html/body/main/div/div[2]/h1");
        public By webinary = By.XPath("/html/body/main/div/div[2]/div/a[1]");
        public By blog = By.XPath("/html/body/main/div/div[2]/div/a[2]");
        //public By glossary = By.XPath("/html/body/main/div/div[2]/div/a[2]");
        //public By getGrandFormPhone = By.XPath("/html/body/main/section[2]/div/form/input[4]");
        public TrainingCenter(IWebDriver driver)   //конструктор
        {
            this._driver = driver;
        }

    }
}
