﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using NUnit.Framework;

namespace test
{
    public class UserMethod
    {
        private IWebDriver _driver;
        public UserMethod(IWebDriver driver)   //конструктор
        {
            this._driver = driver;
        }

          
        // public By aboutUsButton;
        public IWebElement FindElement(By aboutUsButton)  //найти главную надпись на главной странице
        {
            return _driver.FindElement(aboutUsButton);
        }


        public string GetTextFromLabel1(By aboutUsButton)  //получить текст главного Label1
        {
            return FindElement(aboutUsButton).Text;
        }

       
    }
}
